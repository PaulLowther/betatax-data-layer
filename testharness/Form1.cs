﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testharness
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Alphatax.DataAccess.DataLayerService svc = new Alphatax.DataAccess.DataLayerService();

            Alphatax.DataAccess.IForms f = svc.GetFactory().MakeForms();

            Dictionary<string, string> formData = new Dictionary<string, string>();

            formData["taxref1"] = "701";
            formData["taxref2"] = "44444";
            formData["taxref3"] = "55555";

            f.SaveForm(39, 3, formData, false);

            f.GetFormData(39, 3, formData);

            formData.Clear();

            formData["pl.i"] = "69";

            f.SaveForm(39, 3, formData, false);

            string val = f.GetValue(39, "coname");
            val = f.GetValue(39, "ped");
            val = f.GetValue(39, "taxref1");
            val = f.GetValue(39, "bollox");

            f.PutValue(39, "taxref1", "007");
        }
    }
}
