﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Data;

namespace Alphatax.DataAccess.ODBC
{
    public class ODBCDataFactory : IDataFactory
    {
        public IForms MakeForms()
        {
            return new ODBC.Forms();
        }
    }


    public class Forms : IForms
    {
        string ConnectionString = @"Driver={SQL Server};Server=WS00695\SQLSERVER2016;Trusted_Connection=Yes;Database=BETATAX;";

        public string GetForm(int periodId, int formId)
        {
            string jsonForm = "";

            OdbcCommand command = new OdbcCommand("{call LOADFORM (?)}");

            using (OdbcConnection connection = new OdbcConnection(ConnectionString))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@FORMID", formId);

                command.Connection = connection;
                connection.Open();

                OdbcDataReader reader = command.ExecuteReader();

                if(reader.Read())
                {
                    jsonForm = reader.GetString(0);
                }
            }
            
            return jsonForm;
        }


        public bool SaveForm(int periodId, int formId, Dictionary<string, string> formData, bool isFinal)
        {
            bool savedOk = false;
            
            OdbcCommand command = new OdbcCommand("{call AddValue (?,?,?,?,?,?)}");

            using (OdbcConnection connection = new OdbcConnection(ConnectionString))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@userid", OdbcType.Int);
                command.Parameters.Add("@perid", OdbcType.Int);
                command.Parameters.Add("@itemid", OdbcType.VarChar, 12);
                command.Parameters.Add("@ddname", OdbcType.VarChar, 20);
                command.Parameters.Add("@value", OdbcType.VarChar, 300);
                command.Parameters.Add("@finalised", OdbcType.Int);

                command.Connection = connection;

                connection.Open();

                command.Transaction = connection.BeginTransaction();

                int lastVal = formData.Count - 1;
                int currentVal = 0;

                command.Parameters["@userid"].Value = 1;//JUST 1 FOR NOW
                command.Parameters["@perid"].Value = periodId;

                foreach (KeyValuePair<string, string> ddnamVal in formData)
                {
                    string [] ddnameBits = ddnamVal.Key.Split('-');

                    command.Parameters["@ddname"].Value = ddnameBits[0];

                    if (ddnameBits.Count<string>() > 1)
                    {
                        command.Parameters["@itemid"].Value = ddnameBits[1];
                    }
                    else
                    {
                        command.Parameters["@itemid"].Value = "";
                    }

                    command.Parameters["@value"].Value = ddnamVal.Value;

                    if (isFinal)
                    {
                        if (lastVal == currentVal)
                        {
                            command.Parameters["@finalised"].Value = 1;
                        }
                        else
                        {
                            command.Parameters["@finalised"].Value = 0;
                        }

                        ++currentVal;
                    }
                    else
                    {
                        command.Parameters["@finalised"].Value = 0;
                    }

                    command.ExecuteNonQuery();
                }

                command.Transaction.Commit();

                savedOk = true;
            }

            return savedOk;
        }
        public void GetFormData(int periodId, int formId, Dictionary<string, string> formData) 
        {
            formData.Clear();

            OdbcCommand command = new OdbcCommand("{call GetFormData (?,?,?)}");

            using (OdbcConnection connection = new OdbcConnection(ConnectionString))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@userid", 1);
                command.Parameters.AddWithValue("@periodid", periodId);
                command.Parameters.AddWithValue("@formid", formId);

                command.Connection = connection;
                connection.Open();

                OdbcDataReader reader = command.ExecuteReader();

                string itemId;

                while (reader.Read())
                {
                    itemId = reader.GetString(0);

                    if(itemId != " ")
                    {
                        formData[reader.GetString(1) + "-" + itemId] = reader.GetString(2).Replace("<", "").Replace(">", "");
                    }
                    else
                    {
                        formData[reader.GetString(1)] = reader.GetString(2);
                    }
                }
            }
        }

        public string GetValue(int periodId, string ddname)
        {
            //WILL HAVE TO ADD ITEM ID LATER
            string val = "";

            OdbcCommand command = new OdbcCommand("{call GetDDValue (?,?,?)}");

            using (OdbcConnection connection = new OdbcConnection(ConnectionString))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@userid", 1);
                command.Parameters.AddWithValue("@periodid", periodId);
                command.Parameters.AddWithValue("@ddname", ddname);

                command.Connection = connection;
                connection.Open();

                OdbcDataReader reader = command.ExecuteReader();

                if(reader.Read())
                {
                    if(!reader.IsDBNull(0))
                    {
                        val = reader.GetString(0);
                    }
                }
            }

            return val;
        }

        public bool PutValue(int periodId, string ddname, string value)
        {
            bool savedOk = false;

            OdbcCommand command = new OdbcCommand("{call AddValue (?,?,?,?,?,?)}");

            using (OdbcConnection connection = new OdbcConnection(ConnectionString))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@userid", OdbcType.Int);
                command.Parameters.Add("@perid", OdbcType.Int);
                command.Parameters.Add("@itemid", OdbcType.VarChar, 12);
                command.Parameters.Add("@ddname", OdbcType.VarChar, 20);
                command.Parameters.Add("@value", OdbcType.VarChar, 300);
                command.Parameters.Add("@finalised", OdbcType.Int);

                command.Connection = connection;

                connection.Open();

                command.Transaction = connection.BeginTransaction();

                command.Parameters["@userid"].Value = 1;//JUST 1 FOR NOW
                command.Parameters["@perid"].Value = periodId;
                command.Parameters["@ddname"].Value = ddname;
                command.Parameters["@itemid"].Value = "";
                command.Parameters["@value"].Value = value;
                command.Parameters["@finalised"].Value = 0;

                command.ExecuteNonQuery();

                command.Transaction.Commit();

                savedOk = true;
            }

            return savedOk;
        }
    }
}
