﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alphatax.DataAccess
{
    public interface IForms
    {
        string GetForm(int periodId, int formId);
        bool SaveForm(int periodId,  int formId, Dictionary<string, string> formData, bool isFinal);

        void GetFormData(int periodId, int formId, Dictionary<string, string> formData);

        string GetValue(int periodId, string ddname);
        bool PutValue(int periodId, string ddname, string value);
    }

    public interface IDataFactory
    {
        IForms MakeForms();     
    }


    public class DataLayerService
    {
        public IDataFactory GetFactory()
        {
            return new ODBC.ODBCDataFactory();
        }
    }
}
